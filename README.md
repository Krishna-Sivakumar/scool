# SCoOL - Scalable Common Optimization Library

**Authors:**
Zainul Abideen Sayed <zsayed@buffalo.edu>,
Jaroslaw Zola <jaroslaw.zola@hush.com>


**Contributors:**

Matt Ferrera <msferrer@buffalo.edu>,
Raman Ghimire <ramanghi@buffalo.edu>,
Shriram Ravi <shriramr@buffalo.edu>,
Nithin Tellapuri <nithin_sastry@outlook.com>,
Sai Vishwanath Venkatesh <saivishw@buffalo.edu>

**Table of Contents:**

[[_TOC_]]

## About

Optimization problems can be thought of as processes exploring trees or directed (acyclic) graphs that represent specific search spaces. For a given search space, nodes of such graph represent feasible states in that search space. The states in turn encode a partial or a complete solution for which objective function, or its bounding function, should be evaluated. In this formulation, we seek to explore the search space, i.e., traverse graph, in the most efficient way to find a complete and optimal final solution with respect to the objective function. Of course, the exploration process is dynamic in the sense that the graph representing the search space cannot be instantiated *a-priori* but rather has to be discovered as we go.

SCoOL is a simple programming model designed to facilitate and accelerate the search space exploration phase of the optimization processes. The main premise of the model is to allow the end-users to focus on the optimization details (e.g., objective function, constraints for the search space, direction in which search space should be explored, etc.), and to delegate the exploration of the resulting search space to an efficient runtime system. The runtime system itself is abstracted from the end-user, and is akin to the classic Bulk Synchronous Parallel (BSP) model. As in BSP, the exploration proceeds in super-steps, where each super-step is distributed across many processing elements running in parallel. However, SCoOL introduces two additional concepts. First, it allows the end-users to specify and maintain a globally shared state that can be used to track, in a somewhat consistent way, an auxiliary information (for example, about the best discovered solution thus far). Second, it introduces a concept of partitioner. Partitioner is specified by the end-user, and provides hints about how to distribute the search space between processing elements to maximize locality (e.g., the neighboring portions of the search space may be handled by the same element to reduce potential communication overheads due to data exchange between processing elements).

SCoOL provides several ready-to-use runtime implementations that target shared memory systems as well as distributed memory clusters.

**The SCoOL project has been supported by [NSF](https://www.nsf.gov/) under the award [OAC-1845840](https://www.nsf.gov/awardsearch/showAward?AWD_ID=1845840).**


## User Guide

The core SCoOL functionality has been implemented in C++ (we are actively developing Python bindings, see below). In this guide, we assume that you are familiar with basic C++ and have at least some rudimentary understanding of C++ templates.

### Practical Applications

Before you start writing your first application, you may want to [check the examples we prepared](examples/). They are a set of practical applications demonstrating the use of SCoOL.

### Install

SCoOL is a header only library. It means that to start using it, all you need is to access the [`include`](include/) directory in the SCoOL root directory. At the same time, we understand that in many applications you may want to use SCoOL to build a standalone project. For such scenarios, we provide `scool-project-init` in [`tools`](tools/) folder.

To create a new project, simply run `scool-project-init` and pass the name of the directory in which the project should be created followed by the project name. For example, running

```
tools/scool-project-init . toy
```

will create `toy` project in the current directory. The tool creates a stub from which you can start implementing your [`Task`](https://cse.buffalo.edu/~jzola/scool/#File:task.hpp) and [`State`](https://cse.buffalo.edu/~jzola/scool/index.html#File:state.hpp) types, and also copies the required header files, license information, etc. Please note that inside the newly created project, the SCoOL `include/` folder is called `scool/`.

### Illustrative Example

To give you an example of how writing SCoOL application may look like, we implement a toy problem of exhaustive tree-like search space exploration. Our search space is a tree over the set of all possible permutations of $n$ objects (i.e., leaves of the tree store all possible $n!$ permutations). Of course, in reality, when dealing with such a search space we would first think about bounding function to constrain it. As you will see, adding a bounding function will be a simple addition to the basic exploration code.

#### Creating Project

To create the project, we would normally run `tools/scool-project-init` as explained earlier. However, we already provide the `toy` example in [`examples/toy`](examples/toy), which you can follow as we discuss the details below.


#### Defining Task

The first step is to define what we call [`Task`](https://cse.buffalo.edu/~jzola/scool/#File:task.hpp). Task represents a node in a search space, and it describes computations that you want to perform when the node is discovered. The task may store both local and static data. The SCoOL API describes in detail [requirements that a type must fulfill to model the Task](https://cse.buffalo.edu/~jzola/scool/#File:task.hpp).

In our example, we are going to implement `toy_task` in the `toy_task.hpp` file. The stub already provides a summary of the methods and functions we have to provide.

To start, we want our task to store the problem size. Since the problem size is a global property we will make it a static attribute:

```c++
class toy_task {
public:
    inline static int n = 0; // problem size

}; // class toy_task
```

To represent the actual permutation, we are going to use `std::vector`, where at position $i$ in the vector we store $i$-th element of the permutation:


```c++
class toy_task {
public:
    inline static int n = 0; // problem size

    int level = 0;      // permutation size
    std::vector<int> p; // state
}; // class toy_task
```

Notice that we also keep the current size of the permutation in attribute `level`.

The API requirement is that `toy_task` is default-constructible, but it is frequently handy to have an additional constructor to create an initial solution:


```c++
class toy_task {
public:
    inline static int n = 0; // problem size

    int level = 0;      // permutation size
    std::vector<int> p; // state

    explicit toy_task(bool init = false) : level(0), p{} {
        if (init) {
            p.resize(n);
            std::iota(std::begin(p), std::end(p), 0);
        }
    } // toy_task
}; // class toy_task
```
In the constructor, we simply initialize ordering of the elements from which we are going to create permutations.

Now comes the critical part, that is the method `process`. This method specifies what should happen when the search space node is discovered (or processed). The method takes two arguments: one is a reference to the object representing [Context](https://cse.buffalo.edu/~jzola/scool/index.html#File:context.hpp), and the other is a reference to the global shared state (which we are going to define later on). When visiting the state, we usually want to do two things: 1) evaluate objective function and check constraints that would help us to limit the search space, and 2) decide which new states we should visit next. We can express this as follows in the code:

```c++
class toy_task {
public:
    inline static int n = 0; // problem size

    int level = 0;      // permutation size
    std::vector<int> p; // state

    explicit toy_task(bool init = false) : level(0), p{} {
        if (init) {
            p.resize(n);
            std::iota(std::begin(p), std::end(p), 0);
        }
    } // toy_task

    template <typename ContextType, typename StateType>
    void process(ContextType& ctx, StateType& st) {
        if (level == n) {
            // potential final solution, evaluate
            // ...
        } else {
            // let's explore
            toy_task t;

            t.level = level + 1;
            t.p = p;

            for (int i = level; i < n; ++i) {
                std::swap(t.p[level], t.p[i]); // construct another (partial) permutation
                // here we could check if a bounding function holds for t
                // to decide if it should be pushed to explore
                ctx.push(t); // we want to explore t
                std::swap(t.p[level], t.p[i]);
            }
        }
    } // process

}; // class toy_task
```

Here, we interact we the SCoOL runtime system through the `ctx` reference that binds to the appropriate object modeling [Context](https://cse.buffalo.edu/~jzola/scool/index.html#File:context.hpp). The details of this object are not important from the end-user perspective, except that it provides method `push` that allows us to add more tasks to the execution. These tasks are passed to the runtime, and are guaranteed to execute in the subsequent superstep.

Finally, the last critical element we must define is method `merge`. This method is relevant only if the search space is a graph, and given search space state may be discovered via different paths. In such case, semantically equivalent tasks (i.e., tasks representing the same point in the search space) must be resolved into a single task that should be preserved. In our case, since the search space is a tree, we can leave this method empty:

```c++
class toy_task {
public:
    inline static int n = 0; // problem size

    int level = 0;      // permutation size
    std::vector<int> p; // state

    explicit toy_task(bool init = false) : level(0), p{} {
        if (init) {
            p.resize(n);
            std::iota(std::begin(p), std::end(p), 0);
        }
    } // toy_task

    template <typename ContextType, typename StateType>
    void process(ContextType& ctx, StateType& st) {
        st.count++; // update global count of processed tasks

        if (level == n) {
            // potential final solution, evaluate
            // ...
        } else {
            // let's explore
            toy_task t;

            t.level = level + 1;
            t.p = p;

            for (int i = level; i < n; ++i) {
                std::swap(t.p[level], t.p[i]); // construct another (partial) permutation
                // here we could check if a bounding function holds for t
                // to decide if it should be pushed to explore
                ctx.push(t); // we want to explore t
                std::swap(t.p[level], t.p[i]);
            }
        }
    } // process

    void merge(const toy_task& t); // no merging will be needed

}; // class toy_task
```

To finalize our `toy_task` class, we need to implement few additional functions. Because C++ can't provide automatic serialization/deserialization routines for our class, we must provide them by hand (via `operator<<` and `operator>>`). Here we can use simple text or binary serialization, but of course we want to stay light-weight. In addition, we must provide equality operator (`operator==`) and, for some executors, we may be required to provide a hashing function (it is a good practice to always implement hashing function anyway).


#### Defining Shared State

While exploring our search space it is inevitable that we will need to maintain some form of a global information. Typically, we will want to keep track of the best solution discovered thus far, or perhaps some basic statistics about the search process. This functionality is provided to us by the [SharedState concept](https://cse.buffalo.edu/~jzola/scool/#File:state.hpp).

As with Task, to define a shared state we create a custom type. In our example, all we care about is the count of the processed tasks. Thus, we store just one integer attribute (`count`). But in order to enable SCoOL runtime system to optimize how shared state is managed, we need few more ingredients. First, we need method `identity` to set the shared state object into its identity state. For instance, identity for integer counter is simply `0`, so that is what our `identity` method does. Second, we need to tell runtime system how to bring different views of the shared state into one consistent view. This is simply a question of how to reduce two states. Again, in our `toy_state` reduction is straightforward: just add the counts maintained by both copies of the state.

```c++
class toy_state {
public:
    int count = 0; // count of processed tasks

    void identity() { count = 0; }

    void operator+=(const toy_state& st) {
        count += st.count; // reduce the current count with that of st
    } // operator+=

}; // class toy_state
```

#### Setting Executor

Now we are ready to put all things together. The process is quite simple, first we select which executor we want to use. Here executor is the abstraction over the actual runtime environment. SCoOL provides several ready to use executors (you can check the list [here](https://cse.buffalo.edu/~jzola/scool/)) including for Message Passing Interface and for multi-core processors. For the sake of simplicity, in our example we are using the most basic sequential executor. However, because all executors share the same interface, [specified by the Executor concept](https://cse.buffalo.edu/~jzola/scool/#File:executor.hpp), substituting executors is almost seamless.

Once we have executor selected, we create application object by specializing the executor with our task and shared state types. Then, we can start interacting with the executor to realize our complete optimization program. And so, in our `toy_main.cpp`, we create `exec` object from `scool::simple_executor` and we keep advancing supersteps (call to method `step`) until the size of the next superstep is zero. In every iteration we check which superstep we are in (by calling `iteration`). We also access our shared state (call to `state`) to see how many tasks we processed.

```c++
#include "scool/simple_executor.hpp"

#include "toy_task.hpp"
#include "toy_state.hpp"


int main(int argc, char* argv[]) {
    toy_task::n = 3;
    scool::simple_executor<toy_task, toy_state> exec;

    exec.init(toy_task(true), toy_state());

    while (exec.step() > 0) {
        std::cout << "step: " << exec.iteration()
                  << ", processed tasks: " << exec.state().count
                  << std::endl;
    }

    return 0;
} // main
```
It is important to understand that all elements of the execution, like managing tasks, managing shared state, keeping track of iterations, etc. are performed automatically by our runtime system. While that looks simple, keep in mind that our task and shared state code could be combined with more complex executors to achieve completely parallel execution (e.g., in distributed memory). The [`examples/`](examples) folder provides several interesting demos to check out.


#### Adding Partitioner

The role of partitioner is to inform runtime about properties of the search space. Since SCoOL is designed to explore a search space in parallel it means inevitable communication, for example, to move tasks to different processing elements. Using partitioner, you can specify which tasks should be in the same equivalence class, and hence preferably assigned to the same processing element. This in turn will allow runtime to minimize tasks movement. In our toy example, we could request for instance that subtrees that have the same first three elements of the permutation are assigned to the same class (and hence to the same processing element). In the essence, we can think about partitioner as a hashing function that assigns tasks to processing elements. As you could expect, partitioner is a type:

```c++
class toy_partitioner {
public:
    int operator()(const toy_task& t) const {
        int d = 100;
        int h = 0;

        for (int i = 0; (i < 3) && (i < t.level); ++i, d /= 10) {
            h += (d * t.p[i]);
        }

        return h;
    } // operator()
}; // class toy_partitioner
```

To use our partitioner, all we need to do is change slightly the definition of our executor:

```c++
scool::simple_executor<toy_task, toy_state, toy_partitioner> exec;
```


### C++ API Specification

The core SCoOL functionality is implemented in C++. You can find an up-to-date and complete SCoOL API specification at [https://cse.buffalo.edu/~jzola/scool/](https://cse.buffalo.edu/~jzola/scool/). The documentation covers both SCoOL concepts and currently available SCoOL models (e.g., [`mpi_executor`](include/mpi_executor.hpp) for distributed memory systems with the Message Passing Interface).


### Python Bindings

Python bindings, `pySCoOL`, are work in progress and hence not everything may work as expected. The bindings are built on top of [`pybind11`](https://github.com/pybind/pybind11).

To install the SCoOL Python bindings, we first need to update `pybind11` dependency submodule. From the SCoOL project root directory run:

```
git submodule update --init

```

Then, run:

```
pip3 install ./python
```

Alternatively, the binding can also be installed in a virtual environment:

```
python3 -m venv /path/to/new/venv
```

activated with:

```
source /path/to/new/venv
```

and then installed with:

```
python3 -m pip install python-binding
```

#### Python Example

A toy example demonstrating how to use the Python bindings is included in [```python/example/``` folder](python/example/).


#### Defining Task

To define task, we follow the same idea as in C++. We first specify task properties (think task attributes):

```python
class Toy_Properties(Properties):
    n = 4
    level = -1
    p = [i for i in range(n)]
```

Then we define the actual task, together with the required methods. However, currently, when defining task we must explicitly specify what context object we should be using (context is normally exposed by the executor). So for example, because we are using `Simple_Executor`, we also pass `Simple_Context` to the `process` method. We hope this element will get simplified in the future.


```python
class Toy_Task(Task):
    def process(self, context: Simple_Context, state: Toy_State) -> None:
        props: Toy_Properties = self.properties
        props.level += 1

        if (props.level < props.n):
            for i in range(props.level, props.n):
                props.p[i], props.p[props.level] = props.p[props.level], props.p[i]
                new_task = Toy_Task(props)
                context.push(new_task)
                props.p[i], props.p[props.level] = props.p[props.level], props.p[i]

    def merge(self, task: object) -> None:
        pass

```

The interaction with the runtime is again similar to what we have in C++:

```python
properties: Properties = Toy_Properties()

task: Task = Toy_Task(properties)
state: State = Toy_State()

executor: Simple_Executor = Simple_Executor()
executor.init(task, state)

while (executor.step() > 0):
    pass
```


## References

To cite SCoOL, please refer to this repository.
