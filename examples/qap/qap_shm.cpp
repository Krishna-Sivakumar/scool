/***
 *  $Id$
 **
 *  File: qap_shm.cpp
 *  Created: Feb 21, 2019
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2019 SCoRe Group
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of SCoOL.
 */

#include <chrono>
#include <iostream>

#include <omp_executor.hpp>
#include <simple_executor.hpp>

#include "qap_common.hpp"
#include "qap_state.hpp"
#include "qap_task.hpp"


int main(int argc, char* argv[]) {
    if (argc != 2) {
        std::cout << "usage: qap_seq qaplib_instance" << std::endl;
        return 0;
    }

    auto t0 = std::chrono::steady_clock::now();

    if (read_qaplib_instance(argv[1], qap_task::n_, qap_task::F_, qap_task::D_)) {
        //auto [p, cost] = qap_task::init_solution_random(111, 100);
        auto [p, cost] = qap_task::init_solution();

        qap_task t(std::begin(p), std::end(p));
        qap_state st(cost, p);

        scool::experimental::omp_executor<qap_task, qap_state, scool::anti_partitioner<qap_task>, true> exec;
        //scool::simple_executor<qap_task, qap_state, scool::anti_partitioner<qap_task>, true> exec;
        exec.log().level(jaz::Logger::DEBUG);

        exec.init(t, st);

        exec.log().info() << "starting solution:" << std::endl;
        st.print(exec.log().info());

        t0 = std::chrono::steady_clock::now();

        while (exec.step() > 0) { }

        auto t1 = std::chrono::steady_clock::now();

        exec.log().info() << "final result:" << std::endl;
        exec.state().print(exec.log().info());

        std::chrono::duration<double> T = t1 - t0;
        exec.log().info() << "time to solution: " << T.count() << "s" << std::endl;
    } else {
        std::cout << "error: could not read instance" << std::endl;
        return -1;
    }

    auto t2 = std::chrono::steady_clock::now();

    std::chrono::duration<double> T = t2 - t0;
    std::cout << "time: " << T.count() << "s" << std::endl;

    std::_Exit(0);
    return 0;
} // main
