# SCoOL Examples

Here we provide a set of examples demonstrating how to use `SCoOL`. The examples can be used to test the performance of our algorithms.

## What's in the folder

We provide four test cases, each with growing complexity:

* `toy` is a toy example that does not solve any actual problem but rather demonstrates how to develop an application using our programming model.
* `qap` is a fully functional exact QAP solver based on the bounding function by  Pardalos and Crouse (see https://doi.org/10.1145/76263.76302).
* `tsp` is a variant of the 2-OPT heuristic for the TSP problem.
* `bnsl` is a scalable exact Bayesian networks structure search solver based on the OPE technique by Karan and Zola (see https://arxiv.org/abs/1608.02682).
* `mpsb` is a scalable solver for the parrent set assignment problem based on the ideas by Karan and Zola (see https://arxiv.org/abs/1705.06390).

For each problem we provide a set of test input data in the `data/` folder.

## Having fun

Start by building the test cases. To this end, simply run `./build.sh`. Make sure that you have your favorite MPI library properly configured such that `cmake` can find it. If the build process ends without problems, you should see `bin/` folder with ready to use test applications. You can use `CC` and `CXX` variables to change default compiler. For instance, by calling `CC=icc CXX=icpc ./build.sh` you can build test cases using Intel C/C++ compilers.

To build with HPX enabled use `-x` flag
`CC=icc CXX=icpc ./build.sh -x`

We recommend the following two tests to start. Both application use MPI so the actual call may need to be coupled with `mpiexec` or `srun` or similar:

* `./qap_mpi ../data/qap/tai12a.dat` -- since this application runs with MPI, if you are running on a cluster, you may need to integrate this invocation with whatever tools your cluster requires (e.g., `srun`, etc.).
* `./bnsl_mpi 35 ../data/bnsl/mildew.35x160K.mps` -- as previously, you may need to adopt this invocation to your specific cluster.

In addition to the MPI versions, we provide also shared memory realizations of the solvers. For example, `./qap_shm ../data/qap/tai12a.dat` will run the QAP solver but using OpenMP parallelism.
