/***
 *  $Id$
 **
 *  File: bnsl_mpi.cpp
 *  Created: Jan 13, 2022
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *          Zainul Abideen Sayed <zsayed@buffalo.edu>
 *  Copyright (c) 2020-2023 SCoRe Group
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of SCoOL.
 */

#include <chrono>
#include <iostream>
#include <numeric>
#include <vector>

#include <mpi_executor.hpp>
#include <partitioner.hpp>

#include "bnsl_task.hpp"
#include "bnsl_state.hpp"


template<int N> void bnsl_search(MPI_Comm Comm, int n, const std::string& in) {
    using task_type = bnsl_task<N>;
    using partitioner_type = bnsl_hyper_partitioner<N>;

    using set_type = uint_type<N>;

    int rank, size;

    MPI_Comm_rank(Comm, &rank);
    MPI_Comm_size(Comm, &size);

    task_type::n = n;
    task_type t;
    bnsl_state<set_type> st;

    t.mps_list.init(n);

    auto res = task_type::mps_list.read(n, in);

    if (res.first) {
        // initialize remaining part of task_type
        task_type::target_id = set_full<set_type>(n);
        task_type::opt_pa.resize(n);

        for (int xi = 0; xi < n; ++xi) {
            auto opt = task_type::mps_list.optimal(xi);
            task_type::opt_pa[xi] = {opt.pa, opt.s};
        }
    } else {
        if (rank == 0) std::cout << "error: " << res.second << std::endl;
        return;
    }

    // bnsl tasks are never unique, they form poset lattice
    scool::mpi_executor<task_type, bnsl_state<set_type>, partitioner_type, false> exec(Comm);

    // exec.log() = std::move(mpix::Logger(rank, "mpi"));
    exec.log().level(mpix::Logger::DEBUG);

    exec.init(t, st, partitioner_type());
    auto t0 = std::chrono::steady_clock::now();

    exec.log().debug() << "processing with N = " << N << " words of size " << sizeof(bit_util_base_type) << "B" << std::endl;
    exec.log().info() << "MPS has " << task_type::mps_list.size() << " parent sets" << std::endl;

    for (int i = 0; i <= t.n; ++i) {
        int count = exec.step();
        exec.log().debug() << "active tasks: " << exec.state().active_task << std::endl;
        exec.log().debug() << "empty tasks: " << exec.state().empty_task << std::endl;
    }

    auto t1 = std::chrono::steady_clock::now();

    exec.log().info() << "final result:" << std::endl;
    exec.state().print(exec.log().info());

    std::chrono::duration<double> T = t1 - t0;
    exec.log().info() << "time to solution: " << T.count() << "s" << std::endl;
} // bnsl_search


int main(int argc, char* argv[]) {
    int tlevel, size, rank;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &tlevel);

    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (tlevel != MPI_THREAD_MULTIPLE) {
        if (rank == 0) std::cout << "error: insufficient threading support in MPI" << std::endl;
        return MPI_Finalize();
    }

    if (argc != 3) {
        if (rank == 0) std::cout << "usage: bnsl_mpi n mpsfile" << std::endl;
        return MPI_Finalize();
    }

    int n = std::atoi(argv[1]);

    if (n <= set_max_item<1>()) bnsl_search<1>(MPI_COMM_WORLD, n, argv[2]);
    else if (n <= set_max_item<2>()) bnsl_search<2>(MPI_COMM_WORLD, n, argv[2]);
    else if (n <= set_max_item<3>()) bnsl_search<3>(MPI_COMM_WORLD, n, argv[2]);
    else if (n <= set_max_item<4>()) bnsl_search<4>(MPI_COMM_WORLD, n, argv[2]);
    else {
        if (rank == 0) std::cout << "unable to handle more than " << set_max_item<4>() << " variables!";
    }

    return MPI_Finalize();
} // main
