/***
 *  $Id$
 **
 *  File: tsp_hpx_shm.cpp
 *  Created: April 25, 2022
 *
 *  Author: Sai Vishwanath Venkatesh <saivishw@buffalo.edu>
 *          Zainul Abideen Sayed <zsayed@buffalo.edu>
 *  Copyright (c) 2020-2022 SCoRe Group
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of SCoOL.
 */

#include <chrono>
#include <iostream>
#include <numeric>
#include <vector>

#include <hpx_shm_executor.hpp>

#include "tsp_common.hpp"
#include "tsp_state.hpp"
#include "tsp_task.hpp"


int main(int argc, char* argv[]) {
    if (argc != 4) {
        std::cout << "usage: tsp_hpx_shm bf tsplib_instance bucket_size" << std::endl;
        return 0;
    }

    tsp_task::bf_ = std::atoi(argv[1]);
    int bucket_size = std::atoi(argv[3]);

    if (tsp_task::bf_ < 2) {
        std::cout << "error: too small branching factor" << std::endl;
        return -1;
    }

    if (read_tsp_instance(argv[2], tsp_task::n_, tsp_task::D_)) {
        std::vector<int> res(tsp_task::n_);
        std::iota(std::begin(res), std::end(res), 0);

        tsp_task t(std::begin(res), std::end(res));
        tsp_state st;

        scool::hpx_shm_executor<tsp_task, tsp_state, tsp_partitioner, false> exec(bucket_size);

        exec.init(t, st);

        auto t0 = std::chrono::steady_clock::now();

        while (exec.step() > 0) { }

        auto t1 = std::chrono::steady_clock::now();

        exec.log().info() << "result: ";
        exec.state().print(exec.log().info());

        std::chrono::duration<double> T = t1 - t0;
        exec.log().info() << "time to solution: " << T.count() << "s" << std::endl;
    } else {
        std::cout << "error: could not read instance" << std::endl;
        return -1;
    }

    std::_Exit(0);
    return 0;
} // main
