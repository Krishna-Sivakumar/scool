/***
 *  $Id$
 **
 *  File: Priors.hpp
 *  Created: May 28, 2018
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2018 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef PRIORS_HPP
#define PRIORS_HPP

#include <fstream>
#include <vector>

#include <jaz/iterator.hpp>
#include <jaz/string.hpp>

#include "bit_util.hpp"


template <int N>
class Priors {
public:
    using set_type = uint_type<N>;

    enum Result { IGNORE, EXTEND, ADD_AND_EXTEND };

    enum prior_class { EDGE, NOEDGE };

    struct prior_type {
        int s;
        int t;
        prior_class R;
    }; // struct prior_type


    bool empty() const { return allowed_.empty(); }

    int max_pa_size(int xi) const { return required_.empty() ? 0 : set_size(required_[xi]); }

    std::pair<bool, std::string> read(const std::string& name, int n) {
        std::vector<std::string> lines;

        std::ifstream f(name);
        if (!f) return {false, "priors read error"};

        jaz::getline_iterator<> fit(f), end;
        std::copy(fit, end, std::back_inserter(lines));

        auto res = m_parse__(lines, n);
        if (!res.first) return res;

        res = m_validate__(n);
        if (!res.first) return res;

        m_package__(n);

        return {true, ""};
    } // read

    void reorder(const std::vector<int>& order) const {
        int n = allowed_.size();

        // we reverse order (due to fucked up interface, should be fixed at some point)
        std::vector<int> iorder(n);
        for (int i = 0; i < n; ++i) iorder[order[i]] = i;

        // first allowed_
        std::vector<set_type> n_allowed(n);
        std::vector<set_type> n_required(n);

        for (auto& x : n_allowed) x = set_empty<set_type>();
        for (auto& x : n_required) x = set_empty<set_type>();

        for (int i = 0; i < n; ++i) {
            for (int xi = 0; xi < n; ++xi) {
                if (in_set(allowed_[i], xi)) n_allowed[iorder[i]] = set_add(n_allowed[iorder[i]], iorder[xi]);
                if (in_set(required_[i], xi)) n_required[iorder[i]] = set_add(n_required[iorder[i]], iorder[xi]);
            }
        } // for i

        allowed_ = std::move(n_allowed);
        required_ = std::move(n_required);
    } // reorder

    Result check(int xi, const set_type& pa) const {
        if (allowed_.empty()) return ADD_AND_EXTEND;

        if ((pa & allowed_[xi]) != pa) return IGNORE;
        if ((pa & required_[xi]) != required_[xi]) return EXTEND;

        return ADD_AND_EXTEND;
    } // check


    // the list is not reordered (i.e., CQE ordering is not considered)
    // this is original list taken from input file
    const std::vector<prior_type>& list() const { return priors_; }

    std::vector<prior_type> list_compacted() const {
        std::vector<prior_type> L;

        int n = allowed_.size();
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if (i != j) {
                    if (in_set(required_[i], j)) L.push_back(prior_type{j, i, EDGE});
                    else if (!in_set(allowed_[i], j)) L.push_back(prior_type{j, i, NOEDGE});
                }
            }
        }

        return L;
    } // list_compacted


private:
    static prior_class m_str2prior__(const std::string& s) {
        if (s == "-") return EDGE;
        return NOEDGE;
    } // m_str2prior__

    static std::string m_prior2str__(prior_class pc) {
        if (pc == EDGE) return "-";
        return "!";
    } // m_prior2str__

    friend std::ostream& operator<<(std::ostream& os, const prior_type& pt) {
        os << pt.s << " " << Priors::m_prior2str__(pt.R) << " " << pt.t;
        return os;
    } // operator<<


    // false and -1 means error
    bool m_get_token__(std::istream& is, int& res) {
        res = -1;

        std::string s;
        is >> s;

        if (s == "*") return true;

        try {
            res = std::stoi(s);
        } catch(std::exception& e) {
            res = -1;
        }

        return false;
    } // m_get_token__

    std::pair<bool, std::string> m_parse__(std::vector<std::string>& lines, int n) {
        int l = lines.size();
        priors_.clear();

        std::istringstream is;
        std::string s;

        std::vector<int> rem;
        int u, v;

        bool res;

        for (int i = 0; i < l; ++i) {
            // skip empty lines and comments
            if (lines[i].empty()) continue;
            if (lines[i][0] == '#') continue;

            is.str(lines[i]);
            is.clear();

            // first token must be index or *
            res = m_get_token__(is, u);
            if (!res && ((u == -1) || (u > n - 1))) return {false, "incorrect first token in priors, line " + std::to_string(i + 1)};

            // second must be - or !
            is >> s;
            if ((s != "-") && (s != "!")) return {false, "incorrect relation in priors, line " + std::to_string(i + 1)};

            // third token must be index or *
            res = m_get_token__(is, v);
            if (!res && ((v == -1) || (v > n - 1))) return {false, "incorrect third token in priors, line " + std::to_string(i + 1)};

            // extract remaining tokens that must be indexes
            rem.clear();
            std::copy(std::istream_iterator<int>(is), std::istream_iterator<int>(), std::back_inserter(rem));

            if (!is.eof()) return {false, "incorrect tokens in priors, line " + std::to_string(i + 1)};
            if (!rem.empty() && ((u == -1) || (v == -1))) return {false, "unexpected tokens in priors, line " + std::to_string(i + 1)};

            // here we generate all constraints
            if ((u != -1) && (v != -1)) {
                priors_.emplace_back(prior_type{u, v, m_str2prior__(s)});
                for (auto x : rem) priors_.emplace_back(prior_type{u, x, m_str2prior__(s)});
            } else {
                if (u == -1) {
                    for (int x = 0; x < n; ++x) if (x != v) priors_.emplace_back(prior_type{x, v, m_str2prior__(s)});
                } else {
                    for (int x = 0; x < n; ++x) if (x != u) priors_.emplace_back(prior_type{u, x, m_str2prior__(s)});
                }
            }
        } // for i

        return {true, ""};
    } // m_parse__

    std::pair<bool, std::string> m_validate__(int n) {
        allowed_.clear();
        required_.clear();

        // here priors_ should be checked
        // ...

        return {true, ""};
    } // m_validate__

    void m_package__(int n) {
        if (!priors_.empty()) {
            allowed_.resize(n);
            required_.resize(n);

            for (auto& x : allowed_) x = set_full<set_type>(n);
            for (auto& x : required_) x = set_empty<set_type>();

            for (int i = 0; i < n; ++i) allowed_[i] = set_remove(allowed_[i], i);

            for (auto& x : priors_) {
                switch (x.R) {
                  case NOEDGE:
                      if (in_set(required_[x.t], x.s)) {
                          required_[x.t] = set_remove(required_[x.t], x.s);
                      } else allowed_[x.t] = set_remove(allowed_[x.t], x.s);
                      break;
                  case EDGE:
                      if (!in_set(allowed_[x.t], x.s)) {
                          allowed_[x.t] = set_add(allowed_[x.t], x.s);
                      } else {
                          required_[x.t] = set_add(required_[x.t], x.s);
                          allowed_[x.s] = set_remove(allowed_[x.s], x.t);
                      }
                      break;
                } // switch
            } // for x
        } // if
    } // m_package__


    std::vector<prior_type> priors_;

    mutable std::vector<set_type> allowed_;
    mutable std::vector<set_type> required_;

}; // class Priors

#endif // PRIORS_HPP
