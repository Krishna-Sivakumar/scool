/***
 *  $Id$
 **
 *  File: mpsb_task.hpp
 *  Created: Nov 17, 2022
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *          Zainul Abideen Sayed <zsayed@buffalo.edu>
 *  Copyright (c) 2022 - 2023 SCoRe Group
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of SCoOL.
 */

#ifndef MPSB_TASK_HPP
#define MPSB_TASK_HPP

#include <iostream>

#include <jaz/algorithm.hpp>
#include <jaz/hash.hpp>

#include "MDL.hpp"
#include "MPS.hpp"
#include "Priors.hpp"
#include "RadCounter.hpp"
#include "limits.hpp"


template <int N> class mpsb_task {
public:
    using set_type = typename MPS<N>::set_type;
    using priors_type = Priors<N>;

    using mps_node_t = std::pair<int, typename MPS<N>::MPSNode>;

    set_type pa;
    set_type ch;


    mpsb_task(set_type apa = set_empty<set_type>(),
              set_type ach = set_full<set_type>(n_))
        : pa(apa), ch(ach) { }


    template <typename ContextType, typename StateType>
    void process(ContextType& ctx, StateType& st) {
        set_type ch_ext = set_empty<set_type>();

        // check priors constraints
        if (!priors_.empty()) {
            for (int xi = 0; xi < n_; ++xi) {
                if (!in_set(ch, xi)) continue;
                auto action = priors_.check(xi, pa);
                // IGNORE means pa and its supersets violate prior
                // EXTEND means pa violates prios, but its supersets may be ok
                if (action == priors_type::IGNORE) ch = set_remove(ch, xi);
                else if (action == priors_type::EXTEND) {
                    ch = set_remove(ch, xi);
                    ch_ext = set_add(ch_ext, xi);
                }
            }
        } // if priors_

        // here we evaluate (expensive part)
        std::vector<MDL> mdl(set_size(ch), MDL(m_));
        score_.apply(ch, pa, mdl);

        // identify mps update and where to extend
        std::vector<mps_node_t> mps_buffer;

        for (int xi = 0, idx = 0; xi < n_; ++xi) {
            if (!in_set(ch, xi)) continue;
            if (!extend_insert(xi, pa, mdl[idx].score(), st.mps_list, mps_buffer)) ch = set_remove(ch, xi);
            ++idx;
        }

        // if we EXTEND then MPS may not be able to answer all queries d()
        // we fix it by inserting emptyset with infinity
        if (!is_emptyset(ch_ext) && is_emptyset(pa)) {
            for (int xi = 0; xi < n_; ++xi) {
                if (in_set(ch_ext, xi)) mps_buffer.push_back({xi, {SABNA_DBL_INFTY, pa}});
            }
        }

        auto l = ctx.iteration();

        ch = ch | ch_ext;
        ch = ch & md_[l + 1];

        // create children (here we ensure that tasks are unique, i.e., we have a search tree)
        int xi_max = msb(pa);

        for (int xi = xi_max + 1; xi < n_; ++xi) {
            set_type new_ch = set_remove(ch, xi);
            if (!is_emptyset(new_ch)) ctx.push({set_add(pa, xi), new_ch});
        }

        // update MPS list in shared state
        std::sort(std::begin(mps_buffer), std::end(mps_buffer),
                  [](const mps_node_t& x, const mps_node_t& y) { return x.first < y.first; });

        auto first = std::begin(mps_buffer);
        auto last = std::end(mps_buffer);

        while (first != last) {
            auto iter = jaz::range(first, last,
                                   [](const mps_node_t& x, const mps_node_t& y) { return x.first == y.first; });

            auto lf = first;

            for (; lf != iter; ++lf) {
                st.mps_list.insert(lf->first, lf->second.pa, lf->second.s);
            }

            first = iter;
        } // while
    } // process

    // update MPS list and check whether we should be extending
    static bool extend_insert(int xi, const set_type& pa, const MDL::score_type& s,
                              MPSList<N>& mps_list, std::vector<mps_node_t>& mps_buffer) {
        auto res = mps_list.find(xi, pa);
        if (s.first < res.s) mps_buffer.push_back({xi, {s.first, pa}});
        else if (s.first - s.second + H_[xi] >= res.s) return false;
        return true;
    } // extend_insert

    void merge(const mpsb_task& t) { }


    // precompute bounds to use during exploration
    static void set_bounds() {
        precompute_entropy();
        reorder_variables();
        precompure_max_pasize();
    } // set_bounds

    static void precompute_entropy() {
        set_type E = set_empty<set_type>();
        set_type X = set_full<set_type>(n_);

        H_.resize(n_, -1);

        std::vector<MDL> vec_mdl(1, MDL(m_));

        for (int xi = 0; xi < n_; ++xi) {
            score_.apply(set_add(E, xi), set_remove(X, xi), vec_mdl);
            auto res = vec_mdl[0].score();
            H_[xi] = res.second;
        }
    } // precompute_entropy

    static void reorder_variables() {
        struct pnode {
            int xi;
            int ri;
            double H;

            bool operator<(const pnode& rhs) const {
                if (ri != rhs.ri) return ri > rhs.ri;
                return H < rhs.H;
            }
        }; // struct pnode

        set_type E = set_empty<set_type>();
        set_type X = set_full<set_type>(n_);

        norder_.resize(n_, -1);

        std::vector<pnode> v(n_);
        std::vector<MDL> vec_mdl;

        vec_mdl.resize(1, MDL(m_));

        for (int xi = 0; xi < n_; ++xi) {
            score_.apply(set_add(E, xi), set_remove(X, xi), vec_mdl);
            auto res = vec_mdl[0].score();
            v[xi] = pnode{xi, score_.r(xi), res.second};
        }

        std::sort(std::begin(v), std::end(v));

        for (int i = 0; i < n_; ++i) {
            norder_[i] = v[i].xi;
            H_[i] = v[i].H;
        }

        score_.reorder(norder_);
        priors_.reorder(norder_);
    } // reorder_variables

    static void precompure_max_pasize() {
        set_type E = set_empty<set_type>();
        set_type X = set_full<set_type>(n_);

        md_.resize(n_ + 1, E);
        md_[0] = X;

        std::vector<MDL> vec_mdl(n_, MDL(m_));
        score_.apply(X, E, vec_mdl);

        std::vector<std::pair<int, int>> vec_xi_r(n_, {0, 0});
        for (int xi = 0; xi < n_; ++xi) vec_xi_r[xi] = {xi, score_.r(xi)};

        std::sort(vec_xi_r.begin(), vec_xi_r.end(),
                  [] (const std::pair<int, int>& lhs, const std::pair<int, int>& rhs) {
                      if (lhs.second == rhs.second) { return lhs.first < rhs.first;  }
                      return lhs.second < rhs.second;
                  });

        if ((pa_limit_ < 1) || (pa_limit_ > n_ - 1)) pa_limit_ = n_;

        for (int xi = 0; xi < n_; ++xi) {
            double thres = vec_mdl[xi].score().first - H_[xi];
            double val = vec_mdl[xi].score().first - vec_mdl[xi].score().second;

            for (int j = 0, l = 0; (j < n_) && (l < pa_limit_); ++j) {
                int xj = vec_xi_r[j].first;
                int r_xj = vec_xi_r[j].second;
                if (xi != xj) {
                    val *= r_xj;
                    if (val >= thres) break;
                    ++l;
                    md_[l] = set_add(md_[l], xi);
                }
            } // for j
        } // for xi
    } // precompure_max_pasize


    static inline int n_ = 0;
    static inline int m_ = 0;

    static inline int pa_limit_ = -1;

    static inline sabnatk::RadCounter<N> score_;
    static inline Priors<N> priors_;

    // helper data to facilitate bounds calculations
    static inline std::vector<double> H_;
    static inline std::vector<set_type> md_;

    static inline std::vector<int> norder_;

}; // class mpsb_task


template <int N>
bool operator==(const mpsb_task<N>& t1, const mpsb_task<N>& t2) {
    return ((t1.pa == t2.pa) && (t1.ch == t2.ch));
} // operator==

template <int N>
std::ostream& operator<<(std::ostream& os, const mpsb_task<N>& t) {
    os.write(reinterpret_cast<const char*>(&t.pa), sizeof(t.pa));
    os.write(reinterpret_cast<const char*>(&t.ch), sizeof(t.ch));
    return os;
} // operator<<

template <int N>
std::istream& operator>>(std::istream& is, mpsb_task<N>& t) {
    is.read(reinterpret_cast<char*>(&t.pa), sizeof(t.pa));
    is.read(reinterpret_cast<char*>(&t.ch), sizeof(t.ch));
    return is;
} // operator>>


namespace std {
  template <int N> struct hash<mpsb_task<N>> {
      std::size_t operator()(const mpsb_task<N>& t) const noexcept {
          return h(t.pa) ^ jaz::xorshift64star(h(t.ch));;
      } // operator()

      uint_hash h;
  }; // struct hash
} // namespace std

template <int N> struct hash_partitioner {
    explicit hash_partitioner() { }

    unsigned long long operator()(const mpsb_task<N>& t) const {
        return h(t.pa) ^ jaz::xorshift64star(h(t.ch));
    } // operator()

    uint_hash h;
}; // struct hash_partitioner

#endif // MPSB_TASK_HPP
