/***
 *  $Id$
 **
 *  File: toy_hpx_main.cpp
 *  Created: April 25, 2022
 *  Author: Nithin Sastry Tellapuri <ntellapu@buffalo.edu>
 *          Zainul Abideen Sayed <zsayed@buffalo.edu>
 *  Copyright (c) 2020-2022 SCoRe Group
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *  This file is part of SCoOL.
 */

#include <chrono>
#include <iostream>
#include <numeric>

#include <hpx_shm_executor.hpp>
#include <partitioner.hpp>

#include "toy_task.hpp"
#include "toy_state.hpp"


using task_type = toy_task;
using state_type = toy_state;
using partitioner_type = scool::anti_partitioner<task_type>;


int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::cout << "usage: toy_hpx_main n bucket_size" << std::endl;
        return 0;
    }

    long long int count = 0;
    long long int tasks = 0;

    toy_task::n = std::atoi(argv[1]);
    int bucket_size = std::atoi(argv[2]);

    scool::hpx_shm_executor<task_type, state_type, partitioner_type, false> exec(bucket_size);

    exec.init(task_type(true), state_type());

    auto t0 = std::chrono::steady_clock::now();

    do {
        tasks = exec.step();
        count += exec.state().count;
    } while (tasks > 0);

    auto t1 = std::chrono::steady_clock::now();

    exec.log().info() << "total processed tasks: " << count << std::endl;
    std::chrono::duration<double> T = t1 - t0;
    exec.log().info() << "time to process: " << T.count() << "s" << std::endl;

    return 0;
} // main
