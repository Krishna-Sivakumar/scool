/***
 *  $Id$
 **
 *  File: partitioner.hpp
 *  Created: Feb 17, 2020
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2020 SCoRe Group
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of SCoOL.
 */

#ifndef PARTITIONER_HPP
#define PARTITIONER_HPP

namespace scool {

  // Class: anti_partitioner
  // Naive implementation of the <Partitioner> concept that performs no partitioning.
  // Please do not use in production!
  template <typename TaskType>
  struct anti_partitioner {
      // Function: operator()
      //
      // Returns:
      //   always 0.
      int operator()(const TaskType& t) const { return 0; }
  }; // struct anti_partitioner

  // Class: hash_partitioner
  // Basic <Partitioner> based on hashing.
  template <typename TaskType>
  struct hash_partitioner {
      // Function: operator()
      std::size_t operator()(const TaskType& t) const { return h(t); }
      std::hash<TaskType> h;
  }; // struct hash_partitioner

} // namespace scool

#endif // PARTITIONER_HPP
