/***
 *  $Id$
 **
 *  File: mpi_executor.hpp
 *  Created: Feb 04, 2020
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *          Zainul Abideen Sayed <zsayed@buffalo.edu>
 *  Copyright (c) 2020-2023 SCoRe Group
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of SCoOL.
 */

#ifndef MPI_EXECUTOR_HPP
#define MPI_EXECUTOR_HPP

#include <atomic>
#include <cmath>
#include <cstddef>
#include <future>
#include <mutex>
#include <random>
#include <set>
#include <thread>

#include "impl.hpp"
#include "mpi_impl.hpp"
#include "partitioner.hpp"
#include "utility.hpp"

#include "mpix/logger.hpp"
#include "mpix/mpi_types.hpp"


namespace scool {

  using mtx_lock = std::mutex;

  template <typename ExecutorType, bool Unique = true>
  class mpi_context {
  public:
      using task_type = typename ExecutorType::task_type;

      explicit mpi_context(ExecutorType& exec) : exec_(exec) { }

      int iteration() const { return exec_.iteration(); }

      bool push(const task_type& t) {
          if constexpr (Unique) {
              if (exec_.nxt_mtx_.try_lock()) {
                  impl::add_to<Unique>(exec_.next_, t);
                  exec_.nxt_mtx_.unlock();
              } else {
                  impl::add_to<Unique>(exec_.hlp_next_, t);
              }
          } else {
              // group by partitioner
              // TODO: use an efficient mod mapper
              auto rank = exec_.pt_(t) % static_cast<std::size_t>(exec_.size_);

              const std::lock_guard<mtx_lock> lock(exec_.next_mtx_[rank]);
              return impl::add_to<Unique>(exec_.next_[rank], t);
          }

          return true;
      } // push

  private:
      mpi_context(const mpi_context&) = delete;
      void operator=(const mpi_context&) = delete;

      ExecutorType& exec_;

  }; // mpi_context


  // Base class to derive mpi_executor
  // Do not use directly!
  template <typename TaskType, typename StateType, typename Partitioner>
  class mpi_executor_base__ {

  public:
      using task_type = TaskType;
      using state_type = StateType;
      using partitioner = Partitioner;

      using req_data_type = scool::impl::bitmap::storage_type;
      using msg_type = std::vector<req_data_type>;
      using response_type = std::pair<req_data_type, int>;

      // ids encoding requests used by mpi_executor
      // REQ_NONE - empty answer
      // REQ_ASK  - request to steal work
      // REQ_ANS  - answer to steal request
      // REQ_FIN  - notification to finalize execution
      // REQ_RDC  - request to participate in reduction
      // REQ_PULL - request to pull next queue
      // REQ_HLP  - request to ask helper thread to compute
      enum request_type : req_data_type {
          REQ_NONE = 0,
          REQ_FIN  = 1,
          REQ_ASK  = 2,
          REQ_ANS  = 3,
          REQ_RDC  = 4,
          REQ_PULL = 5,
          REQ_HLP  = 6
      }; // enum request_type

      // tags used for identifying background communication
      enum REQUEST_TAGS : int {
          REQ_TAG  = 101,
          ANS_TAG  = 102,
          RDC_TAG  = 103,
          PULL_TAG = 104,
          HLP_TAG  = 105,
          PUSH_TAG = 106
      }; // enum REQUEST_TAGS

      explicit mpi_executor_base__(MPI_Comm Comm = MPI_COMM_WORLD, int seed = -1)
          : Comm_(Comm) {
          MPI_Comm_size(Comm_, &size_);
          MPI_Comm_rank(Comm_, &rank_);

          tokens_.resize(size_);
          vranks_.resize(size_ - 1);

          if (seed == -1) {
              std::random_device rd;
              rng0_.seed(rd());
              rng1_.seed(rd());
          } else {
              rng0_.seed(seed);
              rng1_.seed(seed);
          }

          log_.rank(rank_);

          auto gen = [i = 0, r = rank_]() mutable { return ((i >= r) ? ++i : i++); };
          std::generate(vranks_.begin(), vranks_.end(), gen);
      } // mpi_executor_base__

      virtual ~mpi_executor_base__() {
          // block and wait for everybody else to finish
          MPI_Barrier(Comm_);

          // initiate terminating the helper thread
          m_send_message_head__(REQ_FIN, rank_, Comm_hlp_);

          // now we can finish
          hlp_th_.join();

          MPI_Comm_free(&this->Comm_hlp_);
      } // ~mpi_executor_base__


      // Function: log
      mpix::Logger& log() { return log_; }

      // Function: iteration
      int iteration() const { return giter_; }

      // Function: state
      const state_type& state() { return gst_; }

      // Function: checkpoint
      bool checkpoint(const std::string& path) { return false; }

      // Function: restore
      bool restore(const std::string& path) { return false; }


  protected:
      // logger
      const std::string NAME_{"MPIExecutor"};
      mpix::Logger log_{-1};

      // global attributes
      // total tasks, local tasks, remote tasks, variance
      long long int gcount_[5] = { 0, 0, 0, 0, 0 };

      int giter_ = 0;
      long long int steal_miss_ = 0;

      state_type gst_;
      state_type lst_; // local state
      state_type rst_; // received state

      // tokens to piggyback current stealing state
      scool::impl::bitmap tokens_;
      std::mutex tokens_mtx_;

      std::atomic_flag passive_;
      std::mutex rdc_mtx_;

      // MPI env
      MPI_Comm Comm_;

      int size_;
      int rank_;

      MPI_Comm Comm_hlp_;
      std::thread hlp_th_;

      // list of ranks to steal from
      // also keeps track of empty victims
      std::vector<int> vranks_;

      std::mt19937 rng0_;
      std::mt19937 rng1_;

      // this method reduces gst_ over binary tree
      void m_reduce_and_forward__(MPI_Comm Comm, int target = -1) {
          const std::lock_guard<std::mutex> lock(rdc_mtx_);

          gst_ += rst_;
          rst_.identity();
          lst_.identity();

          if ((rank_ > 0) && !(gst_ == lst_)) {
              if (target == -1) target = (rank_ - 1) >> 1;
              m_send_message_head__(REQ_RDC, target, Comm);
              mpi_impl::serialize_and_send(gst_, target, RDC_TAG, Comm);
              lst_ = gst_;
          } // if rank
      } // m_reduce_and_forward__

      response_type m_token_update__(msg_type msg, int target) {
          tokens_mtx_.lock();

          tokens_.OR(msg.data() + 1);
          if (msg[0] == REQ_NONE) tokens_.set(target);

          tokens_mtx_.unlock();

          return { msg[0], target };
      } // m_token_update__

      // these methods implement basic protocol for task stealing
      // every message contains request id and current status of tokens
      response_type m_receive_message_head__(int Tag, MPI_Comm Comm) {
          msg_type msg(1 + tokens_.storage_size());

          // receiving a request
          MPI_Status stat;
          MPI_Recv(msg.data(), msg.size(), mpix::MPI_Type<req_data_type>(), MPI_ANY_SOURCE, Tag, Comm, &stat);
          int target = stat.MPI_SOURCE;

          return m_token_update__(msg, target);
      } // m_receive_message_head__

      response_type m_receive_message_head__(MPI_Comm Comm) {
          return m_receive_message_head__(REQ_TAG, Comm);
      } // m_receive_message_head__

      void m_send_message_head__(request_type req, int target, int Tag, MPI_Comm Comm) {
          msg_type msg(1 + tokens_.storage_size());

          // request id
          msg[0] = req;

          // tokens
          auto tp = tokens_.storage();

          const std::lock_guard<std::mutex> lock(tokens_mtx_);
          std::copy(tp, tp + tokens_.storage_size(), std::begin(msg) + 1);

          // here we go
          MPI_Send(msg.data(), msg.size(), mpix::MPI_Type<req_data_type>(), target, Tag, Comm);
      } // m_send_message_head__

      void m_send_message_head__(request_type req, int target, MPI_Comm Comm) {
          m_send_message_head__(req, target, REQ_TAG, Comm);
      } // m_send_message_head__

      template <typename Context>
      int m_steal_tasks__(MPI_Comm Comm, Context& ctx) {
          auto vranks = vranks_;

          tokens_mtx_.lock();
          tokens_.set(rank_);
          tokens_mtx_.unlock();

          int count = 0;

          std::vector<task_type> T;
          T.reserve(1024);

          auto out = std::back_inserter(T);
          int end = size_ - 1;

          // it seems we need this to avoid bias in load
          std::uniform_int_distribution<int> udist(0, end - 1);

          while (end > 0) {
              // randomly selects a victim
              int pos = udist(rng0_);
              int target = vranks[pos];

              // abort steal if victim is passive
              tokens_mtx_.lock();
              auto vt = tokens_[target];
              tokens_mtx_.unlock();

              if (vt == true) {
                  end = end - 1;
                  udist = std::uniform_int_distribution<int>(0, end - 1);
                  std::swap(vranks[pos], vranks[end]);
                  continue;
              }

              // send request
              m_send_message_head__(REQ_ASK, target, Comm);
              auto [req, _] = m_receive_message_head__(ANS_TAG, Comm);

              if (req == REQ_ANS) {
                  // receive and deserialize the tasks
                  mpi_impl::receive_and_deserialize<task_type>(out, target, ANS_TAG, Comm);

                  for (auto& x : T) {
                      x.process(ctx, gst_);
                      count++;
                  }

                  T.clear();
              } else {
                  // remove target from consideration
                  end = end - 1;
                  udist = std::uniform_int_distribution<int>(0, end - 1);
                  std::swap(vranks[pos], vranks[end]);
                  steal_miss_++;
              }
          } // while end

          passive_.test_and_set();
          m_reduce_and_forward__(Comm);

          return count;
      } // m_steal_tasks__


  private:
      mpi_executor_base__(const mpi_executor_base__&) = delete;
      void operator=(const mpi_executor_base__&) = delete;

  }; // class mpi_executor_base__


  // General mpi_executor (default choice)

  // Class: mpi_executor
  // Parallel <Executor> model built on top of Message Passing Interface.
  // The execution of supersteps is handled by MPI ranks within specified MPI communicator.
  // The executor uses variety of optimizations, e.g., work stealing, message piggybacking, etc.
  // to efficiently execute superstep and maintain shared state.
  //
  // Parameters:
  //   Unique - if *true*, the search space is assumed to be a tree (i.e., tasks are unique), otherwise it is a graph.
  template <typename TaskType, typename StateType, typename Partitioner = hash_partitioner<TaskType>, bool Unique = false>
  class mpi_executor : public mpi_executor_base__<TaskType, StateType, Partitioner> {
  public:
      // Type: task_type
      // Alias to the user provided *TaskType* type representing a task.
      using task_type = TaskType;

      // Type: state_type
      // Alias to the user provided *StateType* type representing a global shared state.
      using state_type = StateType;

      // Type: partitioner
      // Alias to user provided *Partitioner* type representing a task partitioner.
      using partitioner = Partitioner;

      // Function: mpi_executor
      // Creates and launches mpi_executor runtime.
      //
      // Parameters:
      //   Comm - MPI communicator to that the executor should use for communication.
      //   seed - random number generator seed. The executor uses random numbers in
      //          several different places. The seed should be configured to value
      //          different than -1 only in cases where deterministic behavior
      //          between different executions is desired.
      explicit mpi_executor(MPI_Comm Comm = MPI_COMM_WORLD, int seed = -1)
          : mpi_executor_base__<TaskType, StateType, Partitioner>(Comm, seed), ctx_(*this),
            curr_(this->size_), next_(this->size_), curr_mtx_(this->size_), next_mtx_(this->size_),
            porder_(this->size_), pushed_(this->size_), preq_(this->size_), pulled_(this->size_) {

          // we have separate communicator for requests processing
          MPI_Comm_dup(this->Comm_, &this->Comm_hlp_);
          this->hlp_th_ = std::thread(&mpi_executor::m_request_listener__, this, this->Comm_hlp_);

          MPI_Barrier(this->Comm_);

          this->log().info(this->NAME_) << "ready with " << this->size_ << " ranks" << std::endl;
      } // mpi_executor


      // Function: init
      void init(const task_type& t, const state_type& st, const partitioner& pt = partitioner()) {
          pt_ = pt;
          std::vector<task_type> v{t};
          init(std::begin(v), std::end(v), st);
      } // init

      // Function: init
      template <typename Iter>
      void init(Iter first, Iter last, const state_type& st, const partitioner& pt = partitioner()) {
          pt_ = pt;

          for (; first != last; ++first) {
              auto rank = pt_(*first) % static_cast<std::size_t>(this->size_);
              if (rank == this->rank_) impl::add_to<Unique>(curr_[rank], *first);
          }

          curr_size_ = curr_[this->rank_].size();

          this->gst_ = st;
          this->lst_ = st;
          this->rst_ = st;

          long long int count = curr_[this->rank_].size();
          MPI_Allreduce(&count, &this->gcount_[0], 1, MPI_LONG_LONG_INT, MPI_SUM, this->Comm_);

          MPI_Barrier(this->Comm_);
      } // init


      // Function: step
      long long int step() {
          this->log().info(this->NAME_) << "processing " << this->gcount_[0]
                                        << " tasks, superstep " << this->giter_
                                        << "..." << std::endl;
          MPI_Barrier(this->Comm_);

          long long int global_tasks  = this->gcount_[0];
          long long int count[5] = {0, 0, 0, 0, 0};
          long long int local_task;

          // get global state into identity mode!!!
          this->gst_.identity();
          this->lst_ = this->gst_;
          this->rst_ = this->gst_;
          this->passive_.clear();

          preq_.clear();

          for (auto& x : pushed_) x.clear();
          for (auto& x : pulled_) x.clear();

          pushed_[this->rank_].test_and_set();
          pulled_[this->rank_].test_and_set();

          MPI_Barrier(this->Comm_);

          // process local queue
          count[1] = m_process_local_queue__();

          // go into stealing mode
          this->log().debug(this->NAME_) << "stealing tasks...\n";
          count[2] = this->m_steal_tasks__(this->Comm_hlp_, ctx_);

          count[4] = this->steal_miss_;

          // first pushing to already received requests (async)
          this->log().debug(this->NAME_) << "pushing task for next...\n";
          m_push_next__();

          // pull tasks from neighbors
          this->log().debug(this->NAME_) << "pulling task for next...\n";
          m_pull_next__();

          // check if all task from next frontier has been pushed
          auto pred = [](std::atomic_flag& f) { return f.test() == true; };
          while (!std::all_of(pushed_.begin(), pushed_.end(), pred));

          this->log().debug(this->NAME_) << "completed pushing tasks...\n";

          for (auto& x : curr_) count[0] += x.size();
          curr_size_.store(count[0]);

          // calculate standard deviation
          local_task = count[1] + count[2];

          double mean = static_cast<double>(global_tasks) / this->size_;
          double local_sq_diff = (local_task - mean) * (local_task - mean);

          count[3] = std::llround(local_sq_diff);

          // take care of global state
          MPI_Allreduce(count, this->gcount_, 5, MPI_LONG_LONG_INT, MPI_SUM, this->Comm_);

          double sd = std::sqrt(this->gcount_[3] / this->size_);
          double p_sd = (sd / mean) * 100;

          this->log().debug(this->NAME_) << "local tasks: " << this->gcount_[1]
                                         << ", remote tasks: " << this->gcount_[2]
                                         << ", sd: "<< p_sd <<"%"
                                         << ", miss: " <<  this->gcount_[4]
                                         << std::endl;

          long long int processed_task =  this->gcount_[1] + this->gcount_[2];

          if (processed_task != global_tasks) {
              this->log().error() << "something went very wrong, task numbers mismatch!" << std::endl;
          }

          // get local queues in proper shape
          // at this stage all ranks are in sync
          this->tokens_.reset();

          mpi_impl::broadcast(this->gst_, this->Comm_);

          this->giter_++;

          return this->gcount_[0];
      } // step

  protected:
      friend mpi_context<mpi_executor, Unique>;
      mpi_context<mpi_executor, Unique> ctx_;

  private:
      using local_storage_type = std::vector<phmap::flat_hash_set<task_type>>;
      using set_type = phmap::flat_hash_set<task_type>;

      using req_data_type = scool::impl::bitmap::storage_type;
      using msg_type = std::vector<req_data_type>;
      using response_type = std::pair<req_data_type, int>;

      // merges c2 into c1
      // keeps c1 at the front and avoids duplicate while merging
      // randomly shuffles the newly added elements
      template <typename Container>
      void m_merge_vectors__(Container& c1, Container c2) {
          auto sz = c1.size();
          auto r = this->rank_;
          auto pred = [&c2, &r](int i) { (i < r) ? c2[i] = -1 : c2[i - 1] = -1; };

          std::for_each(c1.begin(), c1.end(), pred);

          std::copy_if(std::make_move_iterator(c2.begin()),
                       std::make_move_iterator(c2.end()),
                       std::back_inserter(c1), [](int i) { return i > -1; });

          std::shuffle(c1.begin() + sz, c1.end(), this->rng0_);
      } // m_merge_vectors__

      // service all the pending pull-req in async
      void m_push_next__() {
          // wait if helper thread is busy (avoids dirty read)
          { const std::lock_guard<mtx_lock> lock(preq_mtx_); }
          for (auto& t : preq_) {
              auto ft = std::async(std::launch::async, &mpi_executor::m_future_push__, this, t, this->PUSH_TAG);
          }
      } // m_push_next__

      void m_pull_next__() {
          int wait_count = 0;

          // re-distrubute the tasks for load balancing
          {
              std::hash<task_type> h;
              for (auto& t : next_[this->rank_]) {
                  const std::lock_guard<mtx_lock> lock(pull_mtx_);
                  auto pos = h(t) % static_cast<std::size_t>(this->size_);
                  impl::add_to<Unique>(curr_[pos], t);
              }

              set_type tmp;
              next_[this->rank_].swap(tmp);
          } // mem release block

          m_merge_vectors__(preq_, this->vranks_);

          int buf = 0;

          for (auto& target : preq_) {
              const std::lock_guard<mtx_lock> lock(pull_mtx_);
              buf = 0;

              if (pulled_[target].test_and_set()) continue;

              this->m_send_message_head__(this->REQ_PULL, target, this->REQ_TAG, this->Comm_hlp_);
              MPI_Recv(&buf, 1, MPI_INT, target, this->PULL_TAG, this->Comm_hlp_, MPI_STATUS_IGNORE);

              if (buf == 1) {
                  mpi_impl::receive_deserialize_add<task_type, Unique>(curr_, target, this->PULL_TAG, this->Comm_hlp_);
              } else wait_count++;
          } // for

          MPI_Status stat;

          while (wait_count > 0) {
              buf = 0;

              MPI_Recv(&buf, 1, MPI_INT, MPI_ANY_SOURCE, this->PUSH_TAG, this->Comm_hlp_, &stat);
              auto target = stat.MPI_SOURCE;
              mpi_impl::receive_deserialize_add<task_type, Unique>(curr_, target, this->PUSH_TAG, this->Comm_hlp_);

              wait_count--;
          } // while
      } // m_pull_next__

      // responds to a pull req from target
      // bi-directionally tries to pull from the same target if pull-req had not
      // already been sent earlier by the main thread
      // rreferences required by async call
      void m_future_push__(int&& target, int&& Tag) {
          set_type tmp;
          int buf = 1;

          MPI_Send(&buf, 1, MPI_INT, target, Tag, this->Comm_hlp_);
          mpi_impl::serialize_and_send(next_[target].begin(), next_[target].end(), target, Tag, this->Comm_hlp_);

          pushed_[target].test_and_set();
          next_[target].swap(tmp);

          if (pull_mtx_.try_lock()) {
              if (!pulled_[target].test_and_set()) {
                  this->m_send_message_head__(this->REQ_PULL, target, this->REQ_TAG, this->Comm_hlp_);
                  MPI_Recv(&buf, 1, MPI_INT, target, this->PULL_TAG, this->Comm_hlp_, MPI_STATUS_IGNORE);
                  mpi_impl::receive_deserialize_add<task_type, Unique>(curr_, target, this->PULL_TAG, this->Comm_hlp_);
              } // if
              pull_mtx_.unlock();
          } // if
      } // m_future_push__

      response_type m_local_helper__(MPI_Comm Comm) {
          MPI_Request request;
          MPI_Status stat;

          int ready = 0;

          msg_type msg(1 + this->tokens_.storage_size());

          MPI_Irecv(msg.data(), msg.size(), mpix::MPI_Type<req_data_type>(), MPI_ANY_SOURCE, this->REQ_TAG, Comm, &request);
          MPI_Test(&request, &ready, &stat);

          // working on local queue from back
          while (curr_size_.load() > 0 && !ready) {
              for (int rank = this->size_ - 1; rank > 0; --rank) {
                  auto pos = porder_[rank];
                  if (curr_mtx_[pos].try_lock()) {
                      if (!curr_[pos].empty()) {

                          auto iter = curr_[pos].begin();
                          auto end = curr_[pos].end();

                          int sz = 0;
                          for (; iter != end; ++sz) {

                              iter->process(ctx_, this->rst_);

                              iter = curr_[pos].erase(iter);
                              count_.fetch_add(1);
                              curr_size_.fetch_sub(1);

                              MPI_Test(&request, &ready, &stat);
                              if (ready) break;
                          } // for

                      } // if not empty

                      curr_mtx_[pos].unlock();
                      if (ready) break;

                      if (curr_size_.load() <= 0) break;
                  } // if try_lock
              } // for rank
          } // while

          if (!ready) MPI_Wait(&request, &stat);

          int target = stat.MPI_SOURCE;
          return this->m_token_update__(msg, target);
      } // m_local_helper__

      // this runs in listener thread
      void m_request_listener__(MPI_Comm Comm) {
          const int NUM_TRY = this->size_ - 1;
          do {
              auto [req, target] = this->m_receive_message_head__(Comm);

              if (req == this->REQ_HLP) {
                  auto [r, t] = m_local_helper__(Comm);
                  req = r, target = t;
              }

              if (req == this->REQ_FIN) break;

              if (req == this->REQ_PULL) {
                  const std::lock_guard<mtx_lock> lock(preq_mtx_);

                  if (!this->passive_.test()) {
                      int buf = 0;
                      preq_.push_back(target);

                      MPI_Send(&buf, 1, MPI_INT, target, this->PULL_TAG, this->Comm_hlp_);
                  } else {
                      // fire and forget, wait is handled based on size of next
                      auto ft = std::async(std::launch::async, &mpi_executor::m_future_push__, this, target, this->PULL_TAG);
                      // TODO: make SYNC_ a runtime flag for memory vs speed trade-off
                      if (SYNC_) ft.wait();
                  }

                  continue;
              } // if this->REQ_PULL

              if (req == this->REQ_RDC) {
                  state_type tmp_st = this->rst_;
                  mpi_impl::receive_and_deserialize(tmp_st, target, this->RDC_TAG, Comm);

                  {
                      const std::lock_guard<mtx_lock> lock(this->rdc_mtx_);
                      this->rst_ += tmp_st;
                  }

                  if (this->passive_.test()) {
                      if (this->rank_ != 0) this->gst_.identity();
                      this->m_reduce_and_forward__(Comm);
                  }

                  continue;
              } // if this->REQ_RDC

              if (req == this->REQ_ASK) {
                  bool ans = false;

                  if (curr_size_.load() > 0) {
                      // perhaps we have something that can be stolen
                      // currently step is simply 1 to probe all p-1 ranks
                      auto step = 1;

                      // let's make several attempts
                      // first is always work local to target (thief)
                      for (int i = 0; i < NUM_TRY; ++i) {
                          int pos = (target + i * step) % (this->size_);

                          if (curr_mtx_[pos].try_lock()) {
                              // by the time we got lock, there may be no more work to do
                              if (curr_[pos].empty()) {
                                  curr_mtx_[pos].unlock();
                                  continue;
                              }

                              // serializing a batch
                              this->m_send_message_head__(this->REQ_ANS, target, this->ANS_TAG, Comm);
                              mpi_impl::serialize_and_send(curr_[pos].begin(), curr_[pos].end(), target, this->ANS_TAG, Comm);

                              int sz = curr_[pos].size();
                              set_type tmp;

                              curr_[pos].clear();
                              curr_[pos].swap(tmp);
                              curr_size_.fetch_sub(sz);

                              curr_mtx_[pos].unlock();

                              ans = true;
                              break;
                          }
                      } // for i
                  } // if curr_size_

                  if (ans == false) this->m_send_message_head__(this->REQ_NONE, target, this->ANS_TAG, Comm);
              } // if req = REQ_ASK

          } while (true);
      } // m_request_listener__

      long long int m_process_local_queue__() {
          count_.store(0);

          // set the order of local processing
          // local work is always first
          std::iota(std::begin(porder_), std::end(porder_), 0);
          std::swap(porder_[0], porder_[this->rank_]);
          std::shuffle(std::begin(porder_) + 1, std::end(porder_), this->rng0_);

          this->m_send_message_head__(this->REQ_HLP, this->rank_, this->Comm_hlp_);

          do {
              for (int rank = 0; rank < this->size_; ++rank) {
                  auto pos = porder_[rank];
                  if (curr_mtx_[pos].try_lock()) {
                      if (!curr_[pos].empty()) {

                          auto iter = curr_[pos].begin();
                          auto end = curr_[pos].end();

                          for (; iter != end; ++iter) {
                              iter->process(ctx_, this->gst_);
                              count_.fetch_add(1);
                          }

                          int sz = curr_[pos].size();
                          set_type tmp;

                          curr_[pos].clear();
                          curr_[pos].swap(tmp);
                          curr_size_.fetch_sub(sz);
                      } // if curr_[pos]

                      curr_mtx_[pos].unlock();

                      if (curr_size_.load() <= 0) break;
                  } // if try_lock
              } // for rank
          } while (curr_size_.load() > 0);

          return count_.load();
      } // m_process_local_queue__

      std::atomic<long long int> count_;
      std::atomic_int_fast32_t curr_size_;

      bool SYNC_ = false;

      std::vector<int> preq_; // pending push req

      mtx_lock preq_mtx_;
      mtx_lock pull_mtx_;

      std::vector<std::atomic_flag> pushed_;
      std::vector<std::atomic_flag> pulled_;

      std::vector<mtx_lock> curr_mtx_;
      std::vector<mtx_lock> next_mtx_;

      local_storage_type curr_;
      local_storage_type next_;

      std::vector<int> porder_;
      Partitioner pt_;

  }; // class mpi_executor


  // specialized mpi_executor for cases where tasks are guaranteed to be unique

  // Class: mpi_executor
  // This is template specialization for when the search space is assumed to be a tree,
  // (i.e., tasks are guaranteed to be unique).
  template <typename TaskType, typename StateType, typename Partitioner>
  class mpi_executor<TaskType, StateType, Partitioner, true>
      : public mpi_executor_base__<TaskType, StateType, Partitioner> {
  public:
      using task_type = TaskType;
      using state_type = StateType;
      using partitioner = Partitioner;

      explicit mpi_executor(MPI_Comm Comm = MPI_COMM_WORLD, int seed = -1)
          : mpi_executor_base__<TaskType, StateType, Partitioner>(Comm, seed), ctx_(*this) {
          // we have separate communicator for requests processing
          MPI_Comm_dup(this->Comm_, &this->Comm_hlp_);

          // helper thread that acts as a request listener
          this->hlp_th_ = std::thread(&mpi_executor::m_request_listener__, this, this->Comm_hlp_);

          // we have to synchronize, wait untill everybody is ready
          MPI_Barrier(this->Comm_);

          this->log().info(this->NAME_) << "ready with " << this->size_ << " ranks" << std::endl;
      } // mpi_executor


      void init(const task_type& t, const state_type& st,
                const partitioner& pt = partitioner()) {
          std::vector<task_type> v{t};
          init(std::begin(v), std::end(v), st);
      } // init

      template <typename Iter>
      void init(Iter first, Iter last, const state_type& st,
                const partitioner& pt = partitioner()) {
          if (this->rank_ == 0) {
              this->gcount_[0] = 0;

              for (; first != last; ++first, ++this->gcount_[0]) {
                  impl::add_to<true>(curr_, *first);
              }
          }

          this->gst_ = st;
          this->lst_ = st;
          this->rst_ = st;

          hlp_pos_ = curr_.size();
          curr_pos_ = 0;

          goal_post_ = std::ceil(LOCAL_QUEUE_SIZE * curr_.size());

          // to avoid data race between early stealing threads
          MPI_Barrier(this->Comm_);

          MPI_Bcast(&this->gcount_, 4, MPI_LONG_LONG_INT, 0, this->Comm_);
      } // init


      long long int step() {
          this->log().info(this->NAME_) << "processing " << this->gcount_[0]
                                        << " tasks, superstep " << this->giter_
                                        << "..." << std::endl;

          long long int global_tasks  = this->gcount_[0];
          long long int count[5] = {0, 0, 0, 0, 0};
          long long int local_task;

          MPI_Barrier(this->Comm_);

          this->gst_.identity();

          this->passive_.clear();
          this->lst_ = this->gst_;
          this->rst_ = this->gst_;

          // process local queue
          count[1] = m_process_local_queue__();

          // go into stealing mode
          // count[2] = 0;
          // if (this->giter_ <= 5)
          count[2] = this->m_steal_tasks__(this->Comm_hlp_, ctx_);

          count[4] = this->steal_miss_;

          //merge the next search frontiers
          next_.insert(next_.end(), hlp_next_.begin(), hlp_next_.end());
          hlp_next_.clear();

          // take care of global state
          count[0] = next_.size();

          // calculate standard deviation
          local_task = count[1] + count[2];

          double mean = static_cast<double>(global_tasks) / this->size_;
          double local_sq_diff = (local_task - mean) * (local_task - mean);

          count[3] = std::llround(local_sq_diff);

          MPI_Barrier(this->Comm_);
          MPI_Allreduce(count, this->gcount_, 5, MPI_LONG_LONG_INT, MPI_SUM, this->Comm_);

          curr_.swap(next_);
          next_.clear();

          hlp_pos_ = curr_.size();
          curr_pos_ = 0;
          goal_post_ = std::ceil(LOCAL_QUEUE_SIZE * curr_.size());

          double sd = std::sqrt(this->gcount_[3] / this->size_);
          double p_sd = (sd / mean) * 100;

          long long int processed_task = this->gcount_[1] + this->gcount_[2];

          if (processed_task != global_tasks) {
              this->log().error() << "something went very wrong, task numbers mismatch!" << std::endl;
          }

          this->log().debug(this->NAME_) << "local tasks: " << this->gcount_[1]
                                         << ", remote tasks: " << this->gcount_[2]
                                         << ", sd: "<< std::setprecision(3) << p_sd << "%"
                                         << ", miss: " <<  this->gcount_[4]
                                         << std::endl;

          { const std::lock_guard<std::mutex> lock(this->rdc_mtx_); }
          MPI_Barrier(this->Comm_);

          // get local queues in proper shape
          this->tokens_.reset();

          mpi_impl::broadcast(this->gst_, this->Comm_);

          this->giter_++;

          return this->gcount_[0];
      } // step

  protected:
      friend mpi_context<mpi_executor, true>;
      mpi_context<mpi_executor, true> ctx_;

  private:
      using local_storage_type = std::vector<task_type>;
      using req_data_type = scool::impl::bitmap::storage_type;
      using response_type = std::pair<req_data_type, int>;
      using msg_type = std::vector<req_data_type>;

      response_type m_local_helper__(MPI_Comm Comm) {
          MPI_Request request;
          MPI_Status stat;

          int ready = 0;

          msg_type msg(1 + this->tokens_.storage_size());
          MPI_Irecv(msg.data(), msg.size(), mpix::MPI_Type<req_data_type>(), MPI_ANY_SOURCE, this->REQ_TAG, Comm, &request);
          MPI_Test(&request, &ready, &stat);

          int pos = 0;

          // working on local queue from back
          while ((hlp_pos_ > goal_post_) && !ready) {

              mtx_.lock();
              // if main thread has reached GP, stop hlp-thread
              if (curr_pos_ >= goal_post_) {
                  mtx_.unlock();
                  break;
              } // if

              count_.fetch_add(1);
              --hlp_pos_;
              pos = hlp_pos_;
              mtx_.unlock();

              curr_[pos].process(ctx_, this->rst_);
              MPI_Test(&request, &ready, &stat);
              if (ready) break;
          } // while

          if (!ready) MPI_Wait(&request, &stat);

          int target = stat.MPI_SOURCE;
          return this->m_token_update__(msg, target);
      } // m_local_helper__

      // this runs in listener thread
      void m_request_listener__(MPI_Comm Comm) {
          do {
              auto [req, target] = this->m_receive_message_head__(Comm);

              if (req == this->REQ_HLP) {
                  auto [r, t] = m_local_helper__(Comm);
                  req = r, target = t;
              }

              if (req == this->REQ_FIN) break;

              if (req == this->REQ_RDC) {
                  this->rdc_mtx_.lock();
                  state_type tmp_st = this->rst_;

                  mpi_impl::receive_and_deserialize(tmp_st, target, this->RDC_TAG, Comm);

                  this->rst_ += tmp_st;
                  this->rdc_mtx_.unlock();

                  if (this->passive_.test()) {
                      if (this->rank_ != 0) this->gst_.identity();
                      this->m_reduce_and_forward__(Comm, 0);
                  }

                  continue;
              }

              if (req == this->REQ_ASK) {
                  // calculating the batch size based on active ranks
                  // more active ranks higher batch size (10.0% -> 1.0%)
                  int count = this->size_ - this->tokens_.count();
                  task_batch_size_ = std::max((count / static_cast<float>(this->size_)) * 0.1, 0.01);

                  mtx_.lock();

                  int batch = std::ceil((hlp_pos_ - goal_post_) * task_batch_size_);
                  int start = hlp_pos_ - batch;

                  if ((start <= goal_post_) || ((start - curr_pos_) < MIN_TASK_BATCH)) {
                      mtx_.unlock();
                      this->m_send_message_head__(this->REQ_NONE, target, this->ANS_TAG, Comm);
                      continue;
                  }

                  int end = hlp_pos_;
                  hlp_pos_ = start;

                  mtx_.unlock();

                  this->m_send_message_head__(this->REQ_ANS, target, this->ANS_TAG, Comm);

                  auto first = std::next(curr_.begin(), hlp_pos_);
                  auto last = std::next(curr_.begin(), end);

                  mpi_impl::serialize_and_send(first, last, target, this->ANS_TAG, Comm);
              } // if req == REQ_ASK

          } while (true);
      } // m_request_listener__


      // this runs in main thread
      long long int m_process_local_queue__() {
          count_.store(0);

          this->m_send_message_head__(this->REQ_HLP, this->rank_, this->Comm_hlp_);

          // 1. process local portion of the queue
          while (curr_pos_ < goal_post_) {
              curr_[curr_pos_].process(ctx_, this->gst_);
              curr_pos_++;
              count_.fetch_add(1);
          }

          // 2. process shared portion of the queue
          int pos = 0;

          while (true) {
              mtx_.lock();
              if (curr_pos_ == hlp_pos_) {
                  mtx_.unlock();
                  break;
              }

              pos = curr_pos_;
              curr_pos_ = std::min(curr_pos_ + MIN_TASK_BATCH, hlp_pos_);

              mtx_.unlock();

              for (; pos < curr_pos_; ++pos) {
                  count_.fetch_add(1);
                  curr_[pos].process(ctx_, this->gst_);
              } // for
          } // while

          return count_.load();
      } // m_process_local_queue__

      std::atomic<long long int> count_;
      // queue indexes
      int goal_post_ = 0;
      int curr_pos_ = 0;
      int hlp_pos_ = 0;

      // stealing chunk size
      float task_batch_size_ = 0.10;

      std::mutex mtx_;
      std::mutex nxt_mtx_;

      // work queues
      local_storage_type curr_;
      local_storage_type next_;
      local_storage_type hlp_next_;

      // local queue size
      const float LOCAL_QUEUE_SIZE = 0.20;

      // local thread - locks 10 tasks at a time
      // helper thread - allow stealing if tasks > MIN_TASK_BATCH
      const int MIN_TASK_BATCH = 10;

  }; // class mpi_executor

} // namespace scool

#endif // MPI_EXECUTOR_HPP
