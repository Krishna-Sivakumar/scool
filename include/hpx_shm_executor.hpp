/***
 *  $Id$
 **
 *  File: hpx_shm_executor.hpp
 *  Created: Apr 1, 2022
 *
 *  Author: Zainul Abideen Sayed <zsayed@buffalo.edu>
 *          Sai Vishwanath Venkatesh <saivishw@buffalo.edu>
 *          Nithin Sastry Tellapuri <nithin_sastry@outlook.com>
 *  Copyright (c) 2020-2023 SCoRe Group
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of SCoOL.
 */

#ifndef HPX_SHM_EXECUTOR_HPP
#define HPX_SHM_EXECUTOR_HPP

#include "impl.hpp"
#include "partitioner.hpp"

#include <hpx/include/lcos.hpp>
#include <hpx/include/util.hpp>
#include <hpx/config.hpp>
#include <hpx/future.hpp>
#include <hpx/hpx_main.hpp>
#include <hpx/mutex.hpp>

#include "jaz/logger.hpp"


namespace scool {

  template <typename ExecutorType, bool Unique>
  class hpx_context {
  public:
      using task_type = typename ExecutorType::task_type;

      explicit hpx_context(ExecutorType& exec) : exec_(exec) { }

      int iteration() const { return exec_.iteration(); }

      // take a task and add it to the execution environment
      bool push(const task_type& t) {
          auto pos = exec_.pt_(t) % static_cast<std::size_t>(exec_.bucket_size_);

          const std::lock_guard<hpx::mutex> lock(exec_.mtx_[pos]);
          return scool::impl::add_to<Unique>(exec_.next_[pos], t);
      } // push

  private:
      hpx_context(const hpx_context&) = delete;
      void operator=(const hpx_context&) = delete;

      ExecutorType& exec_;
  }; // class hpx_context


  // Class: hpx_shm_executor
  // Parallel <Executor> model built on top of HPX.
  // The execution of superstep is handled by multiple async threads managed via HPX.

  // Parameters:
  //   Unique - if *true*, the search space is assumed to be a tree (i.e., tasks are unique), otherwise it is a graph.
  template <typename TaskType, typename StateType, typename Partitioner = hash_partitioner<TaskType>, bool Unique = false>
  class hpx_shm_executor {
  public:
      // Type: task_type
      // Alias to the user provided *TaskType* type representing a task.
      using task_type = TaskType;

      // Type: state_type
      // Alias to the user provided *StateType* type representing a global shared state.
      using state_type = StateType;

      // Type: partitioner
      // Alias to user provided *Partitioner* type representing a task partitioner.
      using partitioner = Partitioner;


      // Function: hpx_shm_executor
      // Creates and launches hpx_shm_executor runtime.
      //
      // Parameters:
      //   bucket_size - ?
      hpx_shm_executor(int bucket_size) : ctx_(*this) {
          bucket_size_ = bucket_size;

          curr_.resize(bucket_size);
          next_.resize(bucket_size);

          std::vector<hpx::mutex> temp(bucket_size);
          mtx_.swap(temp);

          gst_q_.resize(bucket_size);
      } // hpx_shm_executor

      // Function: init
      template <typename Iter>
      void init(Iter first, Iter last, const state_type& st, const partitioner& pt = partitioner()) {
          pt_ = pt;

          // add task to current layer
          for (; first != last; ++first) {
              auto pos = pt_(*first) % static_cast<std::size_t>(bucket_size_);
              scool::impl::add_to<false>(curr_[pos], *first);

              // add initial states
              gst_q_.push_back(st);
          }

          // initalize gst_
          gst_ = st;

          // broadcast gst_
          m_broadcast_states__();
      } // init

      // Function: init
      void init(const task_type& t, const state_type& st, const partitioner& pt = partitioner()) {
          pt_ = pt;
          std::vector<task_type> v{t};
          init(std::begin(v), std::end(v), st);
      } // init

      // Function: iteration
      int iteration() const { return iter_; }

      // Function: state
      const state_type& state() { return gst_; }

      // Function: step
      long long int step() {
          std::vector<int> sizes(bucket_size_);

          long long int count = 0;

          for (int i = 0; i < bucket_size_; ++i) {
              sizes[i] = curr_[i].size();
              count += sizes[i];
          } // for

          log().info(this->NAME_) << "processing " << count << " tasks, "
                                  << "superstep " << iter_
                                  << "..." <<std::endl;

          gst_.identity();
          for (auto& st: gst_q_) st.identity();

          m_process_current__();

          // reduce and broadcast
          m_reduce_states__();

          // exchange the queue and clear for next superstep
          std::swap(curr_, next_);
          iter_++;

          return count;
      } // step

      // Function: log
      jaz::Logger& log() { return log_; }


  protected:
      friend hpx_context<hpx_shm_executor, Unique>;
      hpx_context<hpx_shm_executor, Unique> ctx_;

      jaz::Logger log_;
      const std::string NAME_ = "HPXExecutor";

  private:
      using task_storage_type = typename std::conditional<Unique, std::vector<std::vector<task_type>>,
                                                                  std::vector<phmap::flat_hash_set<task_type>>>::type;
      using state_vector_type = std::vector<state_type>;

      using future_vector_type = std::vector<hpx::future<void>>;
      using mtx_vector_type = std::vector<hpx::mutex>;

      using set_type = typename std::conditional<Unique, std::vector<task_type>,
                                                phmap::flat_hash_set<task_type>>::type;

      void m_broadcast_states__() {
          // global broadcast
          for (auto& st : gst_q_) st = gst_;
      } // m_broadcast_states__

      void m_reduce_states__() {
          // global reduction
          for (auto& st : gst_q_) gst_ += st;

          // global broadcast
          m_broadcast_states__();
      } // m_reduce_states__

      void m_process_wrapper__(int idx, int l) {
          auto it = std::begin(curr_[idx]);
          auto end = std::end(curr_[idx]);

          for (; it != end; ++it) {
              it->process(ctx_, gst_q_[idx]);
          } // for

          set_type tmp;
          curr_[idx].swap(tmp);
      } // m_process_wrapper__

      void m_process_current__() {
          for (int index = 0; index < bucket_size_; ++index) {
              if (curr_[index].size() > 0)
                  futures_.push_back(hpx::async(&hpx_shm_executor::m_process_wrapper__, this, index ,iter_));
          } // for

          // barrier
          hpx::wait_all(futures_);
      } // m_process_current__

      int bucket_size_;

      // global state definition
      state_type gst_;

      state_type st_;
      int iter_ = 0;

      task_storage_type curr_;
      task_storage_type next_;

      future_vector_type futures_;
      state_vector_type gst_q_;
      mtx_vector_type mtx_;
      Partitioner pt_;

  }; // class hpx_shm_executor

}; // namespace scool

#endif // HPX_SHM_EXECUTOR_HPP
