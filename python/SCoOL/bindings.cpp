#include <pybind11/pybind11.h>

#include <functional>

#include "simple_executor.hpp"
#include "partitioner.hpp"

#include "py_simple_executor.hpp"
#include "universal_task.hpp"
#include "universal_state.hpp"

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

namespace py = pybind11;

using simple_partitioner = scool::anti_partitioner<Py_Universal_Task>;
using simple_executor = scool::simple_executor<Py_Universal_Task, Universal_State>;
using simple_context = scool::simple_context<simple_executor, true>;
using iterator = std::vector<Py_Universal_Task>::iterator;

PYBIND11_MODULE(SCoOLBindings, m) {
    m.doc() = "SCoOLBindings Module";

    py::class_<simple_partitioner>(m, "Simple_Partitioner_Binding")
        .def(py::init<>());

    py::class_<Universal_Task, Py_Universal_Task, std::unique_ptr<Universal_Task, py::nodelete>>(m, "Universal_Task")
        .def(py::init<>(), py::return_value_policy::reference_internal)
        .def("py_process", &Universal_Task::py_process)
        .def("py_merge", &Universal_Task::py_merge)
        .def("set_buffer", &Universal_Task::set_buffer)
        .def("get_buffer", &Universal_Task::get_buffer);

    py::class_<Universal_State>(m, "Universal_State")
        .def(py::init<>(), py::return_value_policy::reference)
        .def("identity", &Universal_State::identity)
        .def("set_buffer", &Universal_State::set_buffer)
        .def("get_buffer", &Universal_State::get_buffer);

    py::class_<Py_Simple_Executor>(m, "Simple_Executor_Binding")
        .def(py::init<>())
        .def("init", static_cast<void (simple_executor::*)(iterator, iterator, const Universal_State &, const simple_partitioner &)>(&simple_executor::init<iterator>),
                py::arg("first"),
                py::arg("last"),
                py::arg("st"),
                py::arg("pt") = simple_partitioner())
        .def("init", static_cast<void (simple_executor::*)(const Py_Universal_Task &, const Universal_State &, const simple_partitioner &)>(&simple_executor::init),
                py::arg("t"),
                py::arg("st"),
                py::arg("pt") = simple_partitioner())
        .def("step", &simple_executor::step)
        .def("get_state", &simple_executor::state, py::return_value_policy::reference, py::keep_alive<1, 0>())
        .def("get_context", &Py_Simple_Executor::get_context, py::return_value_policy::reference, py::keep_alive<1, 0>());

    py::class_<simple_context>(m, "Simple_Context_Binding")
        .def(py::init<simple_executor&>(), py::return_value_policy::reference)
        .def("iteration", &simple_context::iteration)
        .def("push", &simple_context::push, py::keep_alive<1, 2>());

#ifdef VERSION_INFO
    m.attr("__version__") = MACRO_STRINGIFY(VERSION_INFO);
#else
    m.attr("__version__") = "dev";
#endif
}
